import java.util.Arrays;
import java.util.Scanner;

public class Main {

//////  PIRVELI DAVALEBA

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("შეიყვანეთ ტექსტი:");
        String text = scanner.nextLine();

        Main palindrome = new Main();

        if (palindrome.isPalindrome(text)) {
            System.out.println("ეს ტექსტი პალინდრომია");
        } else {
            System.out.println("ეს ტექსტი არ არის პალინდრომი");
        }
    }

    public boolean isPalindrome(String text) {
        int i = text.length() - 1;
        int j = 0;
        while (i > j) {
            if (text.charAt(i) != text.charAt(j)) {
                return false;
            }
            i--;
            j++;
        }
        return true;
    }


////// MEORE DAVALEBA

    public static int minSplit (int amount){
        Scanner scanner = new Scanner(System.in);
        System.out.println("შეიყვანეთ ფულის რაოდენობა");
        int raodenoba = Integer.valueOf(scanner.nextLine());
        raodenoba = amount;

        int nashti = 0;
        int ormocdaatiTetri = raodenoba / 50;

        nashti = raodenoba % 50;

        int ociTetri = nashti / 20;

        nashti = nashti % 20;

        int atiTetri = nashti / 10;

        nashti = nashti % 10;

        int xutiTetri = nashti / 5;

        return ormocdaatiTetri + ociTetri + atiTetri + xutiTetri;

    }


////// MESAME DAVALEBA

    public static int notContains(int[] array) {

        int[] x = array;
        int arrayLength = x.length;
        int minDadebiti = 0;

        Arrays.sort(x);


        for (int i = 0; i < arrayLength; i++) {

            if (x[i] > 0) {

                minDadebiti = i;

                break;


            } else {
                continue;
            }


        }

        int[] dadebitimasivi = Arrays.copyOfRange(x, minDadebiti, arrayLength);


        int[] newMassive = new int[arrayLength - minDadebiti];
        int newMassiveLength = newMassive.length;

        for (int i = 1; i < newMassiveLength + 1; ++i) {
            newMassive[i - 1] = i;
        }

        System.out.println(Arrays.toString(newMassive));


        if (dadebitimasivi[0] > 1) {

            return 1;
        }

        for (int i = 0; i < newMassiveLength; i++) {

            if (dadebitimasivi[i] == newMassive[i]) {
                continue;
            } else {

                return newMassive[i];
            }

        }

        return 0;

    }

////// MEOTXE DAVALEBA

    public static Boolean isProperly(String sequence){

        int firstCount=0;
        int lastCount=0;

        if (sequence.substring(0, 1).equals(")")){

            return false;
        }

        for(int i=1 ; i<sequence.length()+1;i++) {

            if (sequence.substring(i-1, i).equals("(")){
                firstCount++;
            }
            if (sequence.substring(i-1, i).equals(")")){
                lastCount++;
            }

            if (lastCount>firstCount) {
                return false;
            }
        }
        if (firstCount==lastCount) {
            return true;
        }
        return false;
    }


////// MEXUTE DAVALEBA


    public static int countVariants(int stairsCount) {

        int num= stairsCount;


        if(num==1 || num ==0) {
            return 1;
        }
        else {
            return countVariants(num-1)+countVariants(num-2);
        }

    }
}

